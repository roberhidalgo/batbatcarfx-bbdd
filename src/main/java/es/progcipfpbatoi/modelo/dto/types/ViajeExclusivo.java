package es.progcipfpbatoi.modelo.dto.types;

import es.progcipfpbatoi.exceptions.ReservaNoValidaException;
import es.progcipfpbatoi.modelo.dto.Reserva;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class ViajeExclusivo extends Viaje {

    public ViajeExclusivo(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion) {
        super(codViaje, propietario, ruta, fechaSalida, duracion);
    }

    public ViajeExclusivo(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int numPlazas) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, numPlazas);
    }

    public ViajeExclusivo(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int numPlazas, EstadoViaje estadoViaje) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, numPlazas, estadoViaje);
    }

    public ViajeExclusivo(int codViaje, String propietario, String ruta, LocalDateTime fechaSalida, long duracion, float precio, int numPlazas, EstadoViaje estadoViaje, ArrayList<Reserva> reservas) {
        super(codViaje, propietario, ruta, fechaSalida, duracion, precio, numPlazas, estadoViaje, reservas);
    }

    @Override
    public String getTypoString() {
        return "Viaje Exclusivo";
    }

    @Override
    public Reserva reservar(String usuario, int numPlazas) throws ReservaNoValidaException {
        Reserva reserva = super.reservar(usuario, numPlazas);
        if (reserva != null) {
            super.estadoViaje = EstadoViaje.CERRADO;
        }
        return reserva;
    }
}
