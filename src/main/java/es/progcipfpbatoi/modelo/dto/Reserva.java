package es.progcipfpbatoi.modelo.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Reserva {

    private String codigoReserva;

    private String usuario;

    private int plazasSolicitadas;

    private LocalDateTime fechaRealizacion;

    public Reserva(String codigoReserva) {
        this(codigoReserva, "xxxx", 0, LocalDateTime.now());
    }

    public Reserva(String codigoReserva, String usuario, int plazasSolicitadas) {
        this(codigoReserva, usuario, plazasSolicitadas, LocalDateTime.now());
    }

    public Reserva(String codigoReserva, String usuario, int plazasSolicitadas, LocalDateTime fechaRealizacion) {
        this.codigoReserva = codigoReserva;
        this.usuario = usuario;
        this.plazasSolicitadas = plazasSolicitadas;
        this.fechaRealizacion = fechaRealizacion;
    }

    public String getCodigoReserva() {
        return codigoReserva;
    }

    public String getUsuario() {
        return usuario;
    }

    public int getPlazasSolicitadas() {
        return plazasSolicitadas;
    }

    public LocalDateTime getFechaRealizacion() {
        return fechaRealizacion;
    }

    public String getFechaRealizacionFormatted() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTimeFormatter.format(this.fechaRealizacion);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reserva)) return false;
        Reserva reserva = (Reserva) o;
        return codigoReserva.equals(reserva.codigoReserva);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoReserva);
    }

    public void setPropietario(String usuario) {
        this.usuario = usuario;
    }

    public void setPlazasSolicitadas(int numPlazas) {
        this.plazasSolicitadas = numPlazas;
    }


    @Override
    public String toString() {
        return "Reserva{" +
                "codigoReserva='" + codigoReserva + '\'' +
                ", usuario='" + usuario + '\'' +
                ", plazasSolicitadas=" + plazasSolicitadas +
                ", fechaRealizacion=" + fechaRealizacion +
                '}';
    }

    public boolean perteneceAlViaje(String codViaje) {
        return codigoReserva.split("-")[0].equals(codViaje);
    }
}
