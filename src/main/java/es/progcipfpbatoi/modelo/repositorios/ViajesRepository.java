package es.progcipfpbatoi.modelo.repositorios;

import es.progcipfpbatoi.exceptions.ReservaNoValidaException;
import es.progcipfpbatoi.exceptions.ViajeNotCancelableException;
import es.progcipfpbatoi.modelo.dao.ReservaDAOInterface;
import es.progcipfpbatoi.modelo.dao.ViajeDAOInterface;
import es.progcipfpbatoi.modelo.dto.Reserva;
import es.progcipfpbatoi.modelo.dto.types.*;
import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.exceptions.ViajeNotFoundException;

import java.util.ArrayList;
import java.util.Set;

public class ViajesRepository {

    private ViajeDAOInterface viajeDAO;
    private ReservaDAOInterface reservaDAO;

    public ViajesRepository(ViajeDAOInterface viajeDAO, ReservaDAOInterface reservaDAO) {
        this.viajeDAO = viajeDAO;
        this.reservaDAO = reservaDAO;
    }

    /**
     * Obtiene el número de viajes total
     * @return
     */
    public int countViajes() {
        return this.viajeDAO.findAll().size();
    }

    /**
     * Añade el viaje proporcionado si éste no existe previamente
     * o lo actualiza en caso de que éste ya exista
     * @param viaje
     */
    public void save(Viaje viaje) {
        ArrayList<Reserva> reservas = viaje.getReservas();
        ArrayList<Reserva> reservasOld = this.reservaDAO.findByTravel(viaje);
        reservasOld.removeAll(reservas);
        for (Reserva reserva: reservasOld) {
            this.reservaDAO.remove(reserva);
        }
        for (Reserva reserva: reservas) {
            this.reservaDAO.save(reserva);
        }
        this.viajeDAO.save(viaje);
    }

    /**
     * Realiza una nueva reserva para el viaje proporcionado a nombre del usuario y para el número de plazas dadas
     * @param viaje
     * @param usuario
     * @param plazas
     * @return
     * @throws ReservaNoValidaException Si la reserva no puede ser efectuada
     */
    public Reserva reservar(Viaje viaje, String usuario, int plazas) throws ReservaNoValidaException {
        Reserva reserva = viaje.reservar(usuario, plazas);
        this.reservaDAO.save(reserva);
        this.viajeDAO.save(viaje);
        return reserva;
    }

    /**
     * Obtiene el próximo número de código de viaje
     * @return
     */
    public int getNextCod() {
        return countViajes() + 1;
    }

    /**
     * Obtiene el viaje asociado al código de viaje proporcionado
     * @param codViaje
     * @return
     * @throws ViajeNotFoundException Si no hay ningún viaje con ese código
     */
    public Viaje getViaje(int codViaje) throws ViajeNotFoundException {
        Viaje viaje = this.viajeDAO.getById(codViaje);
        viaje.setReservas(this.reservaDAO.findByTravel(String.valueOf(codViaje)));
        return viaje;
    }

    /**
     * Obtiene el viaje asociado al código de viaje proporcionado y que pasa por el destino dado
     * @param codViaje
     * @return
     * @throws ViajeNotFoundException Si no hay ningún viaje con ese código y que pase por el destino proporcionado
     */
    public Viaje getViaje(int codViaje, String destino) throws ViajeNotFoundException {
        Viaje viaje = this.viajeDAO.getById(String.valueOf(codViaje), destino);
        viaje.setReservas(this.reservaDAO.findByTravel(String.valueOf(codViaje)));
        return viaje;
    }

    /**
     * Obtiene el viaje modificable asociado con código proporcionado
     * @param codViaje
     * @return
     * @throws ViajeNotFoundException Si no hay ningún viaje modificable con ese código
     */
    public Viaje getViajeModificable(int codViaje) throws ViajeNotFoundException {
        Viaje viaje = this.viajeDAO.getModificableById(String.valueOf(codViaje));
        viaje.setReservas(this.reservaDAO.findByTravel(String.valueOf(codViaje)));
        return viaje;
    }

    /**
     * Obtiene el viaje asociado a un código de reserva
     * @param codReserva
     * @return
     * @throws ViajeNotFoundException Si no hay ningún viaje asociado al código de reserva proporcionado
     * @throws ReservaNotFoundException Si no hay ninguna reserva con ese código de reserva
     */
    public Viaje getViajeConReserva(String codReserva) throws ViajeNotFoundException, ReservaNotFoundException {

        String[] codViajeYcodReserva = codReserva.split("-");
        String idViaje = codViajeYcodReserva[0];

        ArrayList<Reserva> reservas = this.reservaDAO.findByTravel(idViaje);
        Viaje viaje = this.viajeDAO.getById(Integer.parseInt(idViaje));

        for (Reserva reserva: reservas) {
            if (reserva.getCodigoReserva().equals(codReserva)) {
                viaje.setReservas(reservas);
                return viaje;
            }
        }

        throw new ReservaNotFoundException(codReserva);
    }

    /**
     * Obtiene todos los viajes registrados
     * @return
     */
    public Set<Viaje> findAll() {
        Set<Viaje> viajes = this.viajeDAO.findAll();
        setReservasAViajes(viajes);
        return viajes;
    }

    /**
     * Obtiene todos los viajes del tipo proporcionado
     * @param viajeClass
     * @return
     */
    public Set<Viaje> findAll(Class<? extends Viaje> viajeClass) {
        Set<Viaje> viajes = this.viajeDAO.findAll(viajeClass);
        setReservasAViajes(viajes);
        return viajes;
    }

    /**
     * Obtiene todos los viajes en un estado dado
     * @param estadoViaje
     * @return
     */
    public Set<Viaje> findAll(EstadoViaje estadoViaje) {
        Set<Viaje> viajes = this.viajeDAO.findAll(estadoViaje);
        setReservasAViajes(viajes);
        return viajes;
    }

    /**
     * Obtiene todos los viajes que pasen por la ciudad de destino proporcionada
     * @param ciudadDestino
     * @return
     */
    public Set<Viaje> findAll(String ciudadDestino) {
        Set<Viaje> viajes = this.viajeDAO.findAll(ciudadDestino);
        setReservasAViajes(viajes);
        return viajes;
    }

    private void setReservasAViajes(Set<Viaje> viajes) {
        for (Viaje viaje : viajes) {
            viaje.setReservas(this.reservaDAO.findByTravel(String.valueOf(viaje.getCodViaje())));
        }
    }

    /**
     * Modifica la reserva proporcionada, asignando el nombre de usuario y plazas dados
     * @param codReserva
     * @param nombreUsuario
     * @param numPlazas
     * @throws ViajeNotFoundException Si no hay un viaje asociado a la reserva proporcionado
     */
    public void modificarReserva(String codReserva, String nombreUsuario, int numPlazas) throws ViajeNotFoundException{

        Reserva reserva = this.reservaDAO.findById(codReserva);
        Viaje viaje = this.viajeDAO.getById(Integer.parseInt(codReserva.split("-")[0]));
        Reserva reservaModificada = viaje.modificarReserva(reserva, nombreUsuario, numPlazas);
        this.reservaDAO.save(reservaModificada);
    }

    /**
     * Cancela el viaje dado
     * @param codViaje
     * @throws ViajeNotFoundException Si no existe un viaje con ese código
     * @throws ViajeNotCancelableException Si el viaje no permite ser cancelado
     */
    public void cancelarViaje(int codViaje) throws ViajeNotFoundException, ViajeNotCancelableException {
        Viaje viaje = getViaje(codViaje);
        viaje.cancelar();
        this.viajeDAO.save(viaje);
    }
}
