package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.DatabaseConnectionException;
import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.modelo.dto.Reserva;
import es.progcipfpbatoi.modelo.dto.types.Viaje;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FileReservaDAO implements ReservaDAOInterface {

    private static final String FIELD_SEPARATOR = ";";

    private static final String DATABASE_FILE = "resources/database/reservas.txt";

    private static final int COLUM_COD_RESERVA = 0;

    private static final int COLUMN_USUARIO = 1;

    private static final int COLUMN_PLAZAS = 2;

    private static final int COLUMN_FECHA_TIEMPO_REALIZACION = 3;

    private final File file;

    public FileReservaDAO() {
        this.file = new File(DATABASE_FILE);
    }

    public FileReservaDAO(String pathToFile) {
        this.file = new File(getClass().getResource(pathToFile).getFile());
    }

    @Override
    public ArrayList<Reserva> findAll() {
        try {
            ArrayList<Reserva> reservas = new ArrayList<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return reservas;
                    }
                    reservas.add(getReservaFromRegister(reservaRegistro));
                } while (true);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Reserva findById(String codReserva) {
        try {
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return null;
                    }
                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.getCodigoReserva().equals(codReserva)) {
                        return reserva;
                    }
                } while (true);
            }
        }catch (IOException ex) {
            throw new DatabaseConnectionException(ex.getMessage());
        }
    }

    @Override
    public ArrayList<Reserva> findByUser(String user) {
        try {
            ArrayList<Reserva> reservas = new ArrayList<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return reservas;
                    }
                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.getUsuario().equals(user)) {
                        reservas.add(reserva);
                    }
                } while (true);
            }
        } catch (IOException ex) {
            throw new DatabaseConnectionException(ex.getMessage());
        }
    }

    @Override
    public ArrayList<Reserva> findByTravel(Viaje viaje) {
        return findByTravel(String.valueOf(viaje.getCodViaje()));
    }

    @Override
    public ArrayList<Reserva> findByTravel(String codViaje) {
        try {
            ArrayList<Reserva> reservas = new ArrayList<>();
            try (BufferedReader bufferedReader = getReader()) {
                do {
                    String reservaRegistro = bufferedReader.readLine();
                    if (reservaRegistro == null) {
                        return reservas;
                    }
                    Reserva reserva = getReservaFromRegister(reservaRegistro);
                    if (reserva.perteneceAlViaje(codViaje)) {
                        reservas.add(reserva);
                    }
                } while (true);
            }
        } catch(IOException ex) {
            throw new DatabaseConnectionException(ex.getMessage());
        }
    }

    @Override
    public Reserva getById(String codReserva) throws ReservaNotFoundException {
        Reserva reservaBuscada = findById(codReserva);
        if (reservaBuscada == null) {
            throw new ReservaNotFoundException(codReserva);
        }
        return reservaBuscada;
    }

    @Override
    public boolean save(Reserva reserva) {
        try {
            if (findById(reserva.getCodigoReserva()) == null) {
                append(reserva);
                return true;
            }
            return updateOrRemove(reserva, true);
        } catch (IOException ex) {
            throw new DatabaseConnectionException(ex.getMessage());
        }
    }

    @Override
    public boolean remove(Reserva reserva) {
        try {
            boolean removed = false;
            ArrayList<Reserva> reservaslist = findAll();
            try (BufferedWriter bufferedWriter = getWriter(false)) {
                for (Reserva reservaItem : reservaslist) {
                    if (!reservaItem.equals(reserva)) {
                        bufferedWriter.write(getRegisterFromReserva(reservaItem));
                        bufferedWriter.newLine();
                    } else {
                        removed = true;
                    }
                }

                return removed;
            }
        } catch (IOException ex) {
            throw new DatabaseConnectionException(ex.getMessage());
        }
    }

    private void append(Reserva reserva) throws IOException {
        try (BufferedWriter bufferedWriter = getWriter(true)) {
            bufferedWriter.write(getRegisterFromReserva(reserva));
            bufferedWriter.newLine();
        }
    }

    private boolean updateOrRemove(Reserva reserva, boolean update) throws IOException {
        ArrayList<Reserva> reservaslist = findAll();
        try (BufferedWriter bufferedWriter = getWriter(false)) {
            for (Reserva reservaItem : reservaslist) {
                if (!reservaItem.equals(reserva)) {
                    bufferedWriter.write(getRegisterFromReserva(reservaItem));
                    bufferedWriter.newLine();
                } else if (update) {
                    bufferedWriter.write(getRegisterFromReserva(reserva));
                    bufferedWriter.newLine();
                }
            }
        }
        return true;
    }

    private String getRegisterFromReserva(Reserva reserva) {
        String[] fields = new String[4];
        fields[COLUM_COD_RESERVA] = reserva.getCodigoReserva().trim();
        fields[COLUMN_USUARIO] = reserva.getUsuario().trim();
        fields[COLUMN_PLAZAS] = String.valueOf(reserva.getPlazasSolicitadas());
        fields[COLUMN_FECHA_TIEMPO_REALIZACION] =  reserva.getFechaRealizacionFormatted();
        return String.join(FIELD_SEPARATOR, fields);
    }

    private Reserva getReservaFromRegister(String reservaRegister) {
        String[] reservaFields = reservaRegister.split(FIELD_SEPARATOR);
        String codigoViajeReserva = reservaFields[COLUM_COD_RESERVA];
        String nombre = reservaFields[COLUMN_USUARIO];
        int plazasReservadas = Integer.parseInt(reservaFields[COLUMN_PLAZAS]);
        LocalDateTime fechaRealizacion = LocalDateTime.parse(reservaFields[COLUMN_FECHA_TIEMPO_REALIZACION], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return new Reserva(codigoViajeReserva, nombre, plazasReservadas, fechaRealizacion);
    }

    private BufferedWriter getWriter(boolean append) throws IOException {
        return new BufferedWriter(new FileWriter(file, append));
    }

    private BufferedReader getReader() throws IOException {
        return new BufferedReader(new FileReader(file));
    }
}
