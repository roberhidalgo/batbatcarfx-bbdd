package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.modelo.dto.Reserva;
import es.progcipfpbatoi.modelo.dto.types.Viaje;

import java.util.ArrayList;

public interface ReservaDAOInterface {

    ArrayList<Reserva> findAll();

    Reserva findById(String id);

    ArrayList<Reserva> findByUser(String user);

    ArrayList<Reserva> findByTravel(String codViaje);

    ArrayList<Reserva> findByTravel(Viaje viaje);

    Reserva getById(String id) throws ReservaNotFoundException;

    boolean save(Reserva reserva);

    boolean remove(Reserva reserva);

}
