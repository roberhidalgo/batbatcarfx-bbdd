package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.ViajeNotFoundException;
import es.progcipfpbatoi.modelo.dto.types.EstadoViaje;
import es.progcipfpbatoi.modelo.dto.types.Viaje;

import java.util.Set;

public interface ViajeDAOInterface {

    Set<Viaje> findAll();

    Set<Viaje> findAll(String city);

    Set<Viaje> findAll(EstadoViaje estadoViaje);

    Set<Viaje> findAll(Class<? extends Viaje> viajeClass);

    Viaje findById(int codViaje);

    Viaje getById(int codViaje) throws ViajeNotFoundException;

    Viaje getById(String codViaje, String ciudadDestino) throws ViajeNotFoundException;

    Viaje getModificableById(String codViaje) throws ViajeNotFoundException;

    boolean save(Viaje viaje);

    boolean remove(Viaje viaje);

}
