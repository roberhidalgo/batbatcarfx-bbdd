package es.progcipfpbatoi.modelo.dao;

import es.progcipfpbatoi.exceptions.ViajeNotFoundException;
import es.progcipfpbatoi.modelo.dto.types.EstadoViaje;
import es.progcipfpbatoi.modelo.dto.types.Viaje;

import java.util.Set;

public class DatabaseViajeDAO implements ViajeDAOInterface{
    @Override
    public Set<Viaje> findAll() {
        return null;
    }

    @Override
    public Set<Viaje> findAll(String city) {
        return null;
    }

    @Override
    public Set<Viaje> findAll(EstadoViaje estadoViaje) {
        return null;
    }

    @Override
    public Set<Viaje> findAll(Class<? extends Viaje> viajeClass) {
        return null;
    }

    @Override
    public Viaje findById(int codViaje) {
        return null;
    }

    @Override
    public Viaje getById(int codViaje) throws ViajeNotFoundException {
        return null;
    }

    @Override
    public Viaje getById(String codViaje, String ciudadDestino) throws ViajeNotFoundException {
        return null;
    }

    @Override
    public Viaje getModificableById(String codViaje) throws ViajeNotFoundException {
        return null;
    }

    @Override
    public boolean save(Viaje viaje) {
        return false;
    }

    @Override
    public boolean remove(Viaje viaje) {
        return false;
    }
}
