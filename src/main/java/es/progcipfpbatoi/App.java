package es.progcipfpbatoi;

import es.progcipfpbatoi.controlador.ChangeScene;
import es.progcipfpbatoi.controlador.PrincipalController;
import es.progcipfpbatoi.modelo.dao.FileReservaDAO;
import es.progcipfpbatoi.modelo.dao.FileViajeDAO;
import es.progcipfpbatoi.modelo.repositorios.ViajesRepository;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FileReservaDAO fileReservaDAO = new FileReservaDAO();
        FileViajeDAO fileViajeDAO = new FileViajeDAO();
        ViajesRepository viajesRepository = new ViajesRepository(fileViajeDAO, fileReservaDAO);
        PrincipalController principalController = new PrincipalController(viajesRepository);

        ChangeScene.change(stage, principalController, "/vista/principal.fxml");
    }

    public static void main(String[] args) {
        launch();
    }

}