package es.progcipfpbatoi.exceptions;

public class ViajeNotFoundException extends Exception {

    public ViajeNotFoundException(String codViaje) {
         super("El viaje con " + codViaje + " no ha sido encontrado");
    }

}
