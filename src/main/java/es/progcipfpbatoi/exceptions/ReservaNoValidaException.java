package es.progcipfpbatoi.exceptions;

public class ReservaNoValidaException extends RuntimeException {

    public ReservaNoValidaException(String motivo) {
        super("No se ha podido realizar la reserva. Motivo: " + motivo);
    }
}


