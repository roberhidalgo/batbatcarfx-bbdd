package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.modelo.repositorios.ViajesRepository;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

public class PrincipalController implements GenericController{

    private ViajesRepository viajesRepository;

    public PrincipalController(ViajesRepository viajesRepository) {
        this.viajesRepository = viajesRepository;
    }

    @FXML
    public void initialize() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Dialogo de prueba");
        alert.setHeaderText(null);
        alert.setContentText("Mensaje de alerta de prueba. Para que veas cómo se muestra una alerta");
        alert.showAndWait();
    }
}
