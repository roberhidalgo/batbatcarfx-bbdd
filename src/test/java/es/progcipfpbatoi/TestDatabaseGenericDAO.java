package es.progcipfpbatoi;

import es.progcipfpbatoi.services.MySqlConnectionService;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.*;
import java.sql.Connection;
import java.util.Properties;

public class TestDatabaseGenericDAO {
    protected MySqlConnectionService connectionService;

    protected void prepareTestDB() {
        try {
            this.connectionService = new MySqlConnectionService();

            Connection connection = connectionService.getConnection();
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            Reader reader = new BufferedReader(new FileReader(TestDatabaseReservaDAO.class.getResource("/database/import.sql").getFile()));
            scriptRunner.runScript(reader);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

