package es.progcipfpbatoi;

import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.modelo.dao.DatabaseReservaDAO;
import es.progcipfpbatoi.modelo.dto.Reserva;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TestDatabaseReservaDAO extends TestDatabaseGenericDAO {

    private DatabaseReservaDAO dbReservaDAO;

    public TestDatabaseReservaDAO() {
        this.dbReservaDAO = new DatabaseReservaDAO();
    }

    @BeforeEach
    public void createDB() {
        prepareTestDB();
    }

    @AfterEach
    public void closeConnection() {
        this.connectionService.closeConnection();
    }

    @Test
    public void testFindAll() {
        ArrayList<Reserva> reservas = this.dbReservaDAO.findAll();
        assertEquals(reservas.size(), 4);
    }

    @Test
    public void testFindById() {
        Reserva reserva = this.dbReservaDAO.findById("1-1");
        assertNotNull(reserva);
        Reserva reserva2 = this.dbReservaDAO.findById("2-1");
        assertNotNull(reserva2);
        Reserva reserva3 = this.dbReservaDAO.findById("0-1");
        assertNull(reserva3);
    }

    @Test
    public void testFindByUser() {
        ArrayList<Reserva> reservasUsuario = this.dbReservaDAO.findByUser("manolo");
        assertEquals(reservasUsuario.size(), 0);

        ArrayList<Reserva> reservasUsuario2 = this.dbReservaDAO.findByUser("alex");
        assertEquals(reservasUsuario2.size(), 1);

        ArrayList<Reserva> reservasUsuario3 = this.dbReservaDAO.findByUser("roberto");
        assertEquals(reservasUsuario3.size(), 2);
    }

    @Test
    public void testFindByTravel() {
        ArrayList<Reserva> reservasViaje1 = this.dbReservaDAO.findByTravel("1");
        assertEquals(reservasViaje1.size(), 2);
        ArrayList<Reserva> reservasViaje2 = this.dbReservaDAO.findByTravel("2");
        assertEquals(reservasViaje2.size(), 2);
        ArrayList<Reserva> reservasViaje3 = this.dbReservaDAO.findByTravel("3");
        assertEquals(reservasViaje3.size(), 0);
    }

    @Test
    public void testGetById() {

        try {
            String codReserva = "1-1";
            Reserva reserva = this.dbReservaDAO.getById(codReserva);
            assertEquals(codReserva, reserva.getCodigoReserva());

            String codReserva2 = "2-1";
            Reserva reserva2 = this.dbReservaDAO.findById(codReserva2);
            assertEquals(codReserva2, reserva2.getCodigoReserva());

        } catch(ReservaNotFoundException ex) {
            fail();
        }

        assertThrows(ReservaNotFoundException.class, () -> {
            String codReserva = "0-1";
            this.dbReservaDAO.getById(codReserva);
        });
    }

    @Test
    public void testSave() {

        String codigoTest = "3-0";
        Reserva reserva = this.dbReservaDAO.findById(codigoTest);
        assertNull(reserva);
        assertEquals(this.dbReservaDAO.findAll().size(), 4);

        int plazasSolicitadas = 10;
        this.dbReservaDAO.save(new Reserva(codigoTest, "usuarioTest", plazasSolicitadas));
        assertEquals(this.dbReservaDAO.findAll().size(), 5);

        Reserva reserva2 = this.dbReservaDAO.findById(codigoTest);
        assertNotNull(reserva2);
        assertEquals(plazasSolicitadas, reserva2.getPlazasSolicitadas());

        int nuevasPlazasSolicitadas = 5;
        this.dbReservaDAO.save(new Reserva(codigoTest, "nuevoUsuarioTest", nuevasPlazasSolicitadas));
        assertEquals(this.dbReservaDAO.findAll().size(), 5);

        Reserva reserva3 = this.dbReservaDAO.findById(codigoTest);
        assertNotNull(reserva3);
        assertEquals(nuevasPlazasSolicitadas, reserva3.getPlazasSolicitadas());
    }

    @Test
    public void testRemove() {
        String codigoTest = "2-0";
        this.dbReservaDAO.remove(new Reserva(codigoTest));
        assertEquals(this.dbReservaDAO.findAll().size(), 4);

        Reserva reserva2 = this.dbReservaDAO.findById(codigoTest);
        assertNull(reserva2);
    }
}
