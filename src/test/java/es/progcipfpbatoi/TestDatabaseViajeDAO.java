package es.progcipfpbatoi;

import es.progcipfpbatoi.exceptions.ViajeNotFoundException;
import es.progcipfpbatoi.modelo.dao.DatabaseViajeDAO;
import es.progcipfpbatoi.modelo.dto.types.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestDatabaseViajeDAO extends TestDatabaseGenericDAO {
    private DatabaseViajeDAO dbViajeDAO;

    public TestDatabaseViajeDAO() {
        this.dbViajeDAO = new DatabaseViajeDAO();
    }

    @BeforeEach
    public void createDB() {
        prepareTestDB();
    }

    @AfterEach
    public void closeConnection() {
        this.connectionService.closeConnection();
    }

    @Test
    public void testFindAll() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll();
        assertEquals(5, viajes.size());
        int codigoViajeEsperado = 1;
        for (Viaje viaje: viajes) {
            assertEquals(codigoViajeEsperado++, viaje.getCodViaje());
        }
    }

    @Test
    public void testFindCityFound() {
        String ciudad = "Valencia";
        Set<Viaje> viajesHaciaValencia = this.dbViajeDAO.findAll(ciudad);
        assertEquals(viajesHaciaValencia.size(), 2);
    }

    @Test
    public void testFindCityNotFound() {
        String ciudad = "Muro de Alcoy";
        Set<Viaje> viajesHaciaValencia = this.dbViajeDAO.findAll(ciudad);
        assertEquals(viajesHaciaValencia.size(), 0);
    }

    @Test
    public void testFindEstadoAbierto() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll(EstadoViaje.ABIERTO);
        assertEquals(viajes.size(), 3);
        for (Viaje viaje: viajes) {
            assertEquals(viaje.getEstado(),  EstadoViaje.ABIERTO);
        }
    }

    @Test
    public void testFindEstadoCancelado() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll(EstadoViaje.CANCELADO);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertEquals(viaje.getEstado(), EstadoViaje.CANCELADO);
        }
    }

    @Test
    public void testFindEstadoCerrado() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll(EstadoViaje.CERRADO);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertEquals(viaje.getEstado(), EstadoViaje.CERRADO);
        }
    }

    @Test
    public void testFindViajeCancelable() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll(ViajeCancelable.class);
        assertEquals(viajes.size(), 2);
        for (Viaje viaje : viajes) {
            assertInstanceOf(ViajeCancelable.class, viaje);
        }
    }

    @Test
    public void testFindViajeFlexible() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll(ViajeFlexible.class);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertInstanceOf(ViajeFlexible.class, viaje);
        }
    }

    @Test
    public void testFindViajeExclusivo() {
        Set<Viaje> viajes = this.dbViajeDAO.findAll(ViajeExclusivo.class);
        assertEquals(viajes.size(), 1);
        for (Viaje viaje : viajes) {
            assertInstanceOf(ViajeExclusivo.class, viaje);
        }
    }

    @Test
    public void testFindViajeNormal() {
        Set<Viaje> viajesNormales = this.dbViajeDAO.findAll(Viaje.class);
        assertEquals(viajesNormales.size(), 1);
    }

    @Test
    public void testGetByIdExistRegister() {
        int codViaje = 3;
        Viaje viaje = null;
        try {
            viaje = this.dbViajeDAO.getById(codViaje);
        } catch (ViajeNotFoundException e) {
            fail();
        }
        assertEquals(codViaje, viaje.getCodViaje());
    }

    @Test
    public void testGetByIdNotExistRegister() {
        assertThrows(ViajeNotFoundException.class, () -> {
            this.dbViajeDAO.getById(9);
        });
    }

    @Test
    public void testNewSaveInsert() {
        int codigoTest = 6;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new Viaje(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.dbViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    @Test
    public void testNewViajeExclusivoSaveInsert() {
        int codigoTest = 6;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new ViajeExclusivo(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.dbViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    @Test
    public void testNewViajeCancelableSaveInsert() {
        int codigoTest = 6;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new ViajeCancelable(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.dbViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    @Test
    public void testUpdateSaveOK() {
        int codigoTest = 1;
        String propietario = "Arturo";
        String ruta = "Valencia-Alicante";
        LocalDateTime fechaSalida =  LocalDateTime.of(2023, 5, 12, 12, 0, 0, 0);
        int duracion = 120;
        int precio = 30;
        int plazasOfertadas = 5;
        Viaje viajeToSave2 = new Viaje(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
        this.dbViajeDAO.save(viajeToSave2);
        testViajeEquals(codigoTest, propietario, ruta, fechaSalida, duracion, precio, plazasOfertadas);
    }

    private void testViajeEquals(int codigoTest, String propietario, String ruta,
                                 LocalDateTime fechaSalida, int duracion, int precio, int plazasOfertadas) {
        try {
            Viaje viaje = this.dbViajeDAO.getById(codigoTest);
            assertEquals(viaje.getCodViaje(), codigoTest, "codigo no se establece correctamente");
            assertEquals(viaje.getPropietario(), propietario, "propietario no se establece correctamente");
            assertEquals(viaje.getRuta(), ruta, "ruta no se establece correctamente");
            assertTrue(viaje.getFechaSalida().isEqual(fechaSalida), "fecha no se establece correctamente");
            assertEquals(viaje.getDuracion(), duracion, "duracion no se establece correctamente");
            assertEquals(viaje.getPrecio(), precio, "precio no se establece correctamente");
            assertEquals(viaje.getPlazasOfertadas(), plazasOfertadas, "plazas ofertadas no se establece correctamente");
        } catch (ViajeNotFoundException ex) {
            fail();
        }
    }

    @Test
    public void testRemove() {
        int codViajeTest = 1;
        this.dbViajeDAO.remove(new Viaje(codViajeTest));
        Viaje viaje = this.dbViajeDAO.findById(codViajeTest);
        assertNull(viaje, "La función de borrar viaje es incorrecta");
    }
}
