package es.progcipfpbatoi;

import es.progcipfpbatoi.exceptions.ReservaNotFoundException;
import es.progcipfpbatoi.modelo.dao.FileReservaDAO;
import es.progcipfpbatoi.modelo.dto.Reserva;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

public class TestFileReservaDAO extends TestFileGenericDAO{

    private FileReservaDAO fileReservaDAO;

    public static final String[] FILE_INITIAL_REGISTERS = {
            "1-1;antonio;4;2022-04-03 23:56:06",
            "1-2;alex;3;2022-01-12 12:12:22",
            "2-1;roberto;5;2022-04-03 23:56:06",
            "2-2;roberto;5;2022-04-03 23:56:06"
    };

    public static final String PATH_TO_TEST_FILE = "/database/reservas.txt";

    @BeforeEach
    public void prepareTestFile() {
        prepareTestDBFile(PATH_TO_TEST_FILE, FILE_INITIAL_REGISTERS);
    }

    public TestFileReservaDAO() {
        this.fileReservaDAO = new FileReservaDAO(PATH_TO_TEST_FILE);
    }


    @Test
    public void testFindAll() {
        ArrayList<Reserva> reservas = this.fileReservaDAO.findAll();
        assertEquals(reservas.size(), 4);
    }

    @Test
    public void testFindById() {
        Reserva reserva = this.fileReservaDAO.findById("1-1");
        assertNotNull(reserva);
        Reserva reserva2 = this.fileReservaDAO.findById("2-1");
        assertNotNull(reserva2);
        Reserva reserva3 = this.fileReservaDAO.findById("0-1");
        assertNull(reserva3);
    }

    @Test
    public void testFindByUser() {
        ArrayList<Reserva> reservasUsuario = this.fileReservaDAO.findByUser("manolo");
        assertEquals(reservasUsuario.size(), 0);

        ArrayList<Reserva> reservasUsuario2 = this.fileReservaDAO.findByUser("alex");
        assertEquals(reservasUsuario2.size(), 1);

        ArrayList<Reserva> reservasUsuario3 = this.fileReservaDAO.findByUser("roberto");
        assertEquals(reservasUsuario3.size(), 2);
    }

    @Test
    public void testFindByTravel() {
        ArrayList<Reserva> reservasViaje1 = this.fileReservaDAO.findByTravel("1");
        assertEquals(reservasViaje1.size(), 2);
        ArrayList<Reserva> reservasViaje2 = this.fileReservaDAO.findByTravel("2");
        assertEquals(reservasViaje2.size(), 2);
        ArrayList<Reserva> reservasViaje3 = this.fileReservaDAO.findByTravel("3");
        assertEquals(reservasViaje3.size(), 0);
    }

    @Test
    public void testGetById() {

        try {
            String codReserva = "1-1";
            Reserva reserva = this.fileReservaDAO.getById(codReserva);
            assertEquals(codReserva, reserva.getCodigoReserva());

            String codReserva2 = "2-1";
            Reserva reserva2 = this.fileReservaDAO.findById(codReserva2);
            assertEquals(codReserva2, reserva2.getCodigoReserva());

        } catch(ReservaNotFoundException ex) {
            fail();
        }

        assertThrows(ReservaNotFoundException.class, () -> {
            String codReserva = "0-1";
            this.fileReservaDAO.getById(codReserva);
        });
    }

    @Test
    public void testSave() {

        String codigoTest = "3-0";
        Reserva reserva = this.fileReservaDAO.findById(codigoTest);
        assertNull(reserva);
        assertEquals(this.fileReservaDAO.findAll().size(), 4);

        int plazasSolicitadas = 10;
        this.fileReservaDAO.save(new Reserva(codigoTest, "usuarioTest", plazasSolicitadas));
        assertEquals(this.fileReservaDAO.findAll().size(), 5);

        Reserva reserva2 = this.fileReservaDAO.findById(codigoTest);
        assertNotNull(reserva2);
        assertEquals(plazasSolicitadas, reserva2.getPlazasSolicitadas());

        int nuevasPlazasSolicitadas = 5;
        this.fileReservaDAO.save(new Reserva(codigoTest, "nuevoUsuarioTest", nuevasPlazasSolicitadas));
        assertEquals(this.fileReservaDAO.findAll().size(), 5);

        Reserva reserva3 = this.fileReservaDAO.findById(codigoTest);
        assertNotNull(reserva3);
        assertEquals(nuevasPlazasSolicitadas, reserva3.getPlazasSolicitadas());
    }

    @Test
    public void testRemove() {
        String codigoTest = "2-0";
        this.fileReservaDAO.remove(new Reserva(codigoTest));
        assertEquals(this.fileReservaDAO.findAll().size(), 4);

        Reserva reserva2 = this.fileReservaDAO.findById(codigoTest);
        assertNull(reserva2);
    }
}
